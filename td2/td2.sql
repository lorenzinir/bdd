DROP TABLE Promotions CASCADE CONSTRAINT;
DROP TABLE Groupes CASCADE CONSTRAINT;
DROP TABLE Etudiants CASCADE CONSTRAINT;
DROP TABLE Semestres CASCADE CONSTRAINT;
DROP TABLE Modules CASCADE CONSTRAINT;
DROP TABLE Matieres CASCADE CONSTRAINT;
DROP TABLE Notes CASCADE CONSTRAINT;
DROP TABLE Absences CASCADE CONSTRAINT;
DROP TABLE JustificatifsAbsences CASCADE CONSTRAINT;

-- creation des tables

CREATE TABLE Promotions
(idPromotion VARCHAR(5), nomPromotion VARCHAR(25), nbEtudiantsPromotion NUMBER,
 CONSTRAINT pk_Promotions PRIMARY KEY (idPromotion));

CREATE TABLE Groupes
(idGroupe VARCHAR(5), idPromotion VARCHAR(5),
 CONSTRAINT pk_Groupes PRIMARY KEY (idGroupe),
 CONSTRAINT fk_Groupes_idPromotion FOREIGN KEY (idPromotion) REFERENCES Promotions(idPromotion));

CREATE TABLE Etudiants
(idEtudiant VARCHAR(5), nomEtudiant VARCHAR(25), prenomEtudiant VARCHAR(25), sexeEtudiant VARCHAR(1), dateNaissanceEtudiant DATE, idGroupe VARCHAR(5),
 CONSTRAINT pk_Etudiants PRIMARY KEY (idEtudiant),
 CONSTRAINT fk_Etudiants_idGroupe FOREIGN KEY (idGroupe) REFERENCES Groupes(idGroupe));

CREATE TABLE Semestres
(idSemestre VARCHAR(5), dateDebutSemestre DATE, dateFinSemestre DATE, idPromotion VARCHAR(5),
 CONSTRAINT pk_Semestres PRIMARY KEY (idSemestre),
 CONSTRAINT fk_Semestres_idPromotion FOREIGN KEY (idPromotion) REFERENCES Promotions(idPromotion));

CREATE TABLE Modules
(idModule VARCHAR(5), nomModule VARCHAR(25), idSemestre VARCHAR(5), coefficientModule NUMBER,
 CONSTRAINT pk_Modules PRIMARY KEY (idModule),
 CONSTRAINT fk_Modules_idSemestre FOREIGN KEY (idSemestre) REFERENCES Semestres(idSemestre));

CREATE TABLE Matieres
(idMatiere VARCHAR(5), nomMatiere VARCHAR(25), idModule VARCHAR(5), coefficientMatiere NUMBER,
 CONSTRAINT pk_Matieres PRIMARY KEY (idMatiere),
 CONSTRAINT fk_Matieres_idModule FOREIGN KEY (idModule) REFERENCES Modules(idModule));

CREATE TABLE Notes
(idEtudiant VARCHAR(5), idMatiere VARCHAR(5), note NUMBER,
 CONSTRAINT pk_Notes PRIMARY KEY (idEtudiant, idMatiere),
 CONSTRAINT fk_Notes_idEtudiant FOREIGN KEY (idEtudiant) REFERENCES Etudiants(idEtudiant),
 CONSTRAINT fk_Notes_idMatiere FOREIGN KEY (idMatiere) REFERENCES Matieres(idMatiere));

CREATE TABLE Absences
(idAbsence VARCHAR(5), idEtudiant VARCHAR(5), dateHeureDebutAbsence DATE, dateHeureFinAbsence DATE,
 CONSTRAINT pk_Absences PRIMARY KEY (idAbsence),
 CONSTRAINT fk_Absences_idEtudiant FOREIGN KEY (idEtudiant) REFERENCES Etudiants(idEtudiant));

CREATE TABLE JustificatifsAbsences
(idJustificatifAbsence VARCHAR(5), idEtudiant VARCHAR(5), dateDebutJustificatif DATE, dateFinJustificatif DATE, motifJustificatif VARCHAR(50),
 CONSTRAINT pk_Justificatifs PRIMARY KEY (idJustificatifAbsence),
 CONSTRAINT fk_Justificatifs_idEtudiant FOREIGN KEY (idEtudiant) REFERENCES Etudiants(idEtudiant));


--insertion de donn�es dans les tables

INSERT INTO Promotions (idPromotion, nomPromotion, nbEtudiantsPromotion) (SELECT * FROM Palleja.PERI3_Promotions);
INSERT INTO Groupes (idGroupe, idPromotion) (SELECT * FROM Palleja.PERI3_Groupes);
INSERT INTO Etudiants (idEtudiant, nomEtudiant, prenomEtudiant, sexeEtudiant, dateNaissanceEtudiant, idGroupe) (SELECT * FROM Palleja.PERI3_Etudiants);
INSERT INTO Semestres (idSemestre, dateDebutSemestre, dateFinSemestre, idPromotion) (SELECT * FROM Palleja.PERI3_Semestres);
INSERT INTO Modules (idModule, nomModule, idSemestre, coefficientModule) (SELECT * FROM Palleja.PERI3_Modules);
INSERT INTO Matieres (idMatiere, nomMatiere, idModule, coefficientMatiere) (SELECT * FROM Palleja.PERI3_Matieres);
INSERT INTO Notes (idEtudiant, idMatiere, note) (SELECT * FROM Palleja.PERI3_Notes);
INSERT INTO Absences (idAbsence, idEtudiant, dateHeureDebutAbsence, dateHeureFinAbsence) (SELECT * FROM Palleja.PERI3_Absences);
INSERT INTO JustificatifsAbsences (idJustificatifAbsence, idEtudiant, dateDebutJustificatif, dateFinJustificatif, motifJustificatif) (SELECT * FROM Palleja.PERI3_JustificatifsAbsences);

COMMIT;




SET SERVEROUTPUT ON;


DECLARE
    v_nbeleves INT;
BEGIN
    SELECT COUNT(*) INTO v_nbeleves
    FROM ETUDIANTS
    WHERE idGroupe = 'Q1';
    DBMS_OUTPUT.PUT_LINE ('il y a '||v_nbeleves || ' etudiants dans le groupe q1');

end;


DECLARE

v_nbeleves INT;
v_nomGroupe GROUPES.idGroupe%TYPE := 'Q2';
BEGIN
    SELECT COUNT(*) INTO v_nbeleves
    FROM ETUDIANTS
    WHERE idGroupe = v_nomGroupe;
    DBMS_OUTPUT.PUT_LINE ('il y a '||v_nbeleves || ' etudiants dans le groupe '|| v_nomGroupe);

end;

DECLARE

    v_nbeleves INT;
    v_nomGroupe GROUPES.idGroupe%TYPE := 'Q2';
BEGIN
    SELECT idGroupe INTO v_nomGroupe
    FROM GROUPES
    WHERE idGroupe = v_nomGroupe;

    SELECT COUNT(*) INTO v_nbeleves
    FROM ETUDIANTS
    WHERE idGroupe = v_nomGroupe;
    DBMS_OUTPUT.PUT_LINE ('il y a '||v_nbeleves || ' etudiants dans le groupe '|| v_nomGroupe);

    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN DBMS_OUTPUT.PUT_LINE ('Groupe pas existant');

end;



DECLARE

    v_nbeleves INT;
    v_nomGroupe GROUPES.idGroupe%TYPE := 'Q2';
    v_groupeexiste INT;
BEGIN
    SELECT COUNT(*) INTO v_groupeexiste
    FROM GROUPES
    WHERE idGroupe = v_nomGroupe;

    IF v_groupeexiste = 0
        THEN DBMS_OUTPUT.PUT_LINE ('Groupe pas existant');
    ELSE
        SELECT COUNT(*) INTO v_nbeleves
        FROM ETUDIANTS
        WHERE idGroupe = v_nomGroupe;
        DBMS_OUTPUT.PUT_LINE ('il y a '||v_nbeleves || ' etudiants dans le groupe '|| v_nomGroupe);
    END IF;



end;

DECLARE
    v_etu Etudiants%rowtype;
BEGIN
    SELECT * INTO v_etu
    FROM Etudiants
    WHERE idEtudiant = 'E1';
    DBMS_OUTPUT.PUT_LINE (v_etu.idEtudiant || v_etu.nomEtudiant || v_etu.prenomEtudiant || v_etu.sexeEtudiant || v_etu.dateNaissanceEtudiant || v_etu.idGroupe );
end;

create or replace function nbEtudiantsParGroupe(p_idGroupe IN Groupes.idGroupe%TYPE)
RETURN NUMBER IS
    v_nbeleves INT;
    v_nomGroupe GROUPES.idGroupe%TYPE := p_idGroupe;

    BEGIN
        SELECT idGroupe INTO v_nomGroupe
        FROM GROUPES
        WHERE idGroupe = v_nomGroupe;

        SELECT COUNT(*) INTO v_nbeleves
        FROM ETUDIANTS
        WHERE idGroupe = p_idGroupe;
        return v_nbeleves;

        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN RETURN null ;

    end;


create or replace function nbEtudiantsParPromotion(p_idPromotion in Promotions.idPromotion%TYPE)
RETURN number is
    v_nbEtudiantParProm INT;
    BEGIN
        select sum(nbEtudiantsParGroupe(idGroupe)) INTO v_nbEtudiantParProm
        from GROUPES
        WHERE idPromotion = p_idPromotion;
    return v_nbEtudiantParProm;
    end;

UPDATE PROMOTIONS SET nbEtudiantsPromotion = nbEtudiantsParPromotion('A2')
WHERE idPromotion = 'A2'



create or replace PROCEDURE affichageInfosEtudiant(p_idEtudiant IN Etudiants.idEtudiant%TYPE) is
    v_etu Etudiants%rowtype;
BEGIN
    SELECT * INTO v_etu
    FROM Etudiants
    WHERE idEtudiant = p_idEtudiant;
    DBMS_OUTPUT.PUT_LINE (v_etu.idEtudiant || v_etu.nomEtudiant || v_etu.prenomEtudiant || v_etu.sexeEtudiant || v_etu.dateNaissanceEtudiant || v_etu.idGroupe );
end;




create or replace PROCEDURE miseAJourCoefficientModules is
     v_coeffTot INT;
     v_idModule MODULES.IDMODULE%type ;
     begin
     UPDATE Modules set coefficientModule = (SELECT sum(coefficientMatiere)
                                              from MATIERES
                                              where MATIERES.idModule = MODULES.idModule);
     end;




create or replace PROCEDURE affichageNotesEtudiant(p_idEtudiant IN Etudiants.idEtudiant%TYPE) is
    BEGIN
        FOR v_ligne IN (SELECT nomMatiere, note
                        from NOTES
                        join MATIERES ON MATIERES.idMatiere = NOTES.idMatiere
                        where idEtudiant = p_idEtudiant) loop
             DBMS_OUTPUT.PUT_LINE ( v_ligne.nomMatiere || ' : ' || v_ligne.note );
        end loop ;
    END;


create or replace procedure affichageNotesEtudiantSemestre(p_idEtudiant IN Etudiants.idEtudiant%TYPE,p_idSemestre IN Semestres.idSemestre%TYPE) is
    v_inscris INT ;


    begin
        select count (*) into v_inscris
        from ETUDIANTS e
        join GROUPES g on g.IDGROUPE = e.IDGROUPE
        join SEMESTRES s on s.IDPROMOTION = g.IDPROMOTION
        where e.idEtudiant = p_idEtudiant AND s.idSemestre = P_IDSEMESTRE;

        IF v_inscris = 0
        then DBMS_OUTPUT.PUT_LINE ( 'etu pas inscris ');
        ELSE
        for v_ligne in (SELECT nomMatiere, note
                        from NOTES n
                        join MATIERES m on m.idMatiere = n.idMatiere
                        where idEtudiant = p_idEtudiant AND n.idMatiere in (SELECT IDMATIERE
                                                                        from MATIERES
                                                                        join MODULES on MODULES.idModule = MATIERES.idModule
                                                                        where  idSemestre = p_idSemestre)
                                                                         ) loop

              DBMS_OUTPUT.PUT_LINE ( v_ligne.nomMatiere || ' : ' || v_ligne.note );
              end loop ;
        end if;
    end;

create or replace PROCEDURE affichageToutEtudiantSemestre(p_idEtudiant IN Etudiants.idEtudiant%TYPE,p_idSemestre IN Semestres.idSemestre%TYPE) is
    begin
         affichageInfosEtudiant (p_idEtudiant);
         affichageNotesEtudiantSemestre (p_idEtudiant,p_idSemestre);
    end;


create or replace PROCEDURE affichageAbsencesParPromotion(p_idPromotion IN Promotions.idPromotion%TYPE) is
 begin
 for v_ligne in (select g.idGroupe
                from  GROUPES g
                where idPromotion = p_idPromotion)loop
                DBMS_OUTPUT.PUT_LINE ('GROUPE : ' || v_ligne.IDGROUPE);
                for v_etudiant in (select e.nomEtudiant,e.prenomEtudiant, count (a.IDABSENCE) as nbAbsence
                                from ETUDIANTS e
                                 left join ABSENCES a on a.IDETUDIANT = e.IDETUDIANT
                                 where e.IDGROUPE = v_ligne.IDGROUPE
                                 group by e.NOMETUDIANT, e.PRENOMETUDIANT, e.IDETUDIANT
                                 order by nbAbsence desc ) loop
                         DBMS_OUTPUT.PUT_LINE (v_etudiant.NOMETUDIANT || '  ' || v_etudiant.PRENOMETUDIANT || ' a ete absent ' || v_etudiant.nbAbsence );
                         end loop;

                end loop;
 end;


create or replace function moyenneEtudiantModule (p_etudiant IN Etudiants.idEtudiant%TYPE, p_idModule IN Modules.idModule%TYPE) RETURN NUMBER
is
    v_coeffTotal Number;
    v_noteTotal Number;
begin
    SELECT sum(n.note * M.coefficientMatiere) into v_noteTotal
    FROM Notes n
    JOIN Matieres M on M.idMatiere = n.idMatiere
    join Modules M2 on M2.idModule = M.idModule
    WHERE idEtudiant = p_etudiant and p_idModule = M2.idModule;

    SELECT SUM(M.coefficientMatiere)
    INTO v_coeffTotal
    FROM Matieres M
    WHERE M.idModule = p_idModule;

    return v_noteTotal / v_coeffTotal;

end;

CREATE OR REPLACE FUNCTION valideEtudiantModule(p_idEtudiant IN Etudiants.idEtudiant%TYPE, p_idModule IN Modules.idModule%TYPE) RETURN NUMBER IS
    v_moyenne NUMBER;
BEGIN
    SELECT moyenneEtudiantModule(p_idEtudiant, p_idModule) INTO v_moyenne FROM DUAL;

    IF v_moyenne >= 8 THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;
END;


CREATE OR REPLACE FUNCTION moyenneEtudiantSemestreSansAbs ( p_idEtudiant IN Etudiants.idEtudiant%TYPE, p_idSemestre IN Semestres.idSemestre%TYPE) RETURN NUMBER IS
    v_moyenneModuleTotal NUMBER;
    v_coeffModuleTotal NUMBER;

BEGIN
    v_moyenneModuleTotal := 0;
    v_coeffModuleTotal := 0;
    DBMS_OUTPUT.PUT_LINE (v_coeffModuleTotal || '  ' || v_moyenneModuleTotal);

    FOR v_ligne IN (SELECT  M.idModule, M.coefficientModule
                    FROM Modules M WHERE idSemestre = p_idSemestre) LOOP
        v_moyenneModuleTotal := v_moyenneModuleTotal + moyenneEtudiantModule(p_idEtudiant,v_ligne.idModule);
        IF v_ligne.coefficientModule IS NULL then
            v_coeffModuleTotal := v_coeffModuleTotal + 1;
        ELSE
            v_coeffModuleTotal := v_coeffModuleTotal + v_ligne.coefficientModule;
        end if;
        DBMS_OUTPUT.PUT_LINE (v_coeffModuleTotal || '  ' || v_moyenneModuleTotal);

        END LOOP;
    RETURN v_moyenneModuleTotal / v_coeffModuleTotal;
END;


 










































