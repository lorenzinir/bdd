package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class StockageRessourceDatabase implements Stockage {
    @Override
    public void create(Object element) {


    }

    @Override
    public void update(Object element) {

    }

    @Override
    public void deleteById(int id) {

    }

    @Override
    public Object getById(int id) {
        Ressource r = null;
        SQLUtils utils = SQLUtils.getInstance();
        Connection connection = utils.getConnection();
        String requete = "SELECT idRessource,nom FROM RessourcesOGE WHERE idRessource = id";
        try (
                Statement st = connection.createStatement();
                ResultSet result = st.executeQuery(requete);
                ){
            result.next();
            int idR = result.getInt(1);
            String nom  = String.valueOf(result.getInt(2));
            r = new Ressource(nom);
            r.setIdRessource(idR);
        } catch (SQLException e){
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public List getAll() {
        return null;
    }
}
