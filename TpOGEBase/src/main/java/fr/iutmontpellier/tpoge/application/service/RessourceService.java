package fr.iutmontpellier.tpoge.application.service;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.stub.StockageRessourceStub;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de service qui permet de gérer différentes ressources (matières)
 * Singleton
 */
public class RessourceService {

    private final static StockageRessourceStub stockage = new StockageRessourceStub();

    private final static RessourceService INSTANCE = new RessourceService();

    private RessourceService() {}

    public static RessourceService getInstance() {
        return INSTANCE;
    }

    /**
     * Instancie un objet {@link Ressource} puis le sauvegarde dans la source de données via le repository
     * @param nom : Nom de la {@link Ressource} à créer
     */
    public void createRessource(String nom) {
        stockage.create(new Ressource(nom));

    }

    /**
     * Récupère une instance de {@link Ressource} depuis la source de données, met à jour son nom puis enregistre la
     * mise à jour de l'entité via le repository
     * @param idRessource : identifiant de la {@link Ressource} à mettre à jour
     * @param nom : nouveau nom pour la {@link Ressource}
     */
    public void updateRessource(int idRessource, String nom) {
        Ressource res = stockage.getById(idRessource);
        res.setNom(nom);
        stockage.update(res);

    }

    /**
     * Supprime une {@link Ressource} sur la source de données via le repository
     * @param idRessource : identifiant de la {@link Ressource} à supprimer
     */
    public void deleteRessource(int idRessource) {
        stockage.deleteById(idRessource);
    }

    /**
     * Récupère une instance de {@link Ressource} depuis la source de données via le repository
     * @param idRessource : identifiant de la {@link Ressource} à récupérer
     * @return L'instance de {@link Ressource} correspondant à l'identifiant
     */
    public Ressource getRessource(int idRessource) {
        return stockage.getById(idRessource);
    }

    /**
     * Récupère une liste de toutes les {@link Ressource} depuis la source de données via le repository
     * @return La liste de toutes les {@link Ressource}
     */
    public List<Ressource> getRessources() {
        return stockage.getAll();
    }
}
