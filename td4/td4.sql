









DROP TABLE Travailler CASCADE CONSTRAINTS;
DROP TABLE EtreAffecte CASCADE CONSTRAINTS;
DROP TABLE ProjetsExternes CASCADE CONSTRAINTS;
DROP TABLE ProjetsInternes CASCADE CONSTRAINTS;
DROP TABLE Equipes CASCADE CONSTRAINTS;
DROP TABLE Salaries CASCADE CONSTRAINTS;

/* CREATION DES TABLES */

CREATE TABLE Salaries
(codeSalarie VARCHAR(5), nomSalarie VARCHAR(25), prenomSalarie VARCHAR(25), nbTotalJourneesTravail NUMBER,
 CONSTRAINT pk_Salaries PRIMARY KEY (codeSalarie));

CREATE TABLE Equipes
(codeEquipe VARCHAR(5), nomEquipe VARCHAR(25), codeSalarieChef VARCHAR(5),
 CONSTRAINT pk_Equipes PRIMARY KEY (codeEquipe),
 CONSTRAINT fk_Equipes_codeSalarieChef FOREIGN KEY (codeSalarieChef) REFERENCES Salaries(codeSalarie));

CREATE TABLE ProjetsExternes
(codeProjet VARCHAR(5), nomProjet VARCHAR(60), villeProjet VARCHAR(25), clientProjet VARCHAR(25), codeEquipe VARCHAR(25),
 CONSTRAINT nn_ProjetsExternes CHECK (codeEquipe IS NOT NULL),
 CONSTRAINT pk_ProjetsExternes PRIMARY KEY (codeProjet),
 CONSTRAINT fk_ProjetsExternes_codeEquipe FOREIGN KEY (codeEquipe) REFERENCES Equipes(codeEquipe));

CREATE TABLE ProjetsInternes
(codeProjet VARCHAR(5), nomProjet VARCHAR(60), serviceProjet VARCHAR(25), codeSalarieResponsable VARCHAR(5),
 CONSTRAINT nn_ProjetsInternes CHECK (codeSalarieResponsable IS NOT NULL),
 CONSTRAINT pk_ProjetsInternes PRIMARY KEY (codeProjet),
 CONSTRAINT fk_ProjetsInternes_codeEquipe FOREIGN KEY (codeSalarieResponsable) REFERENCES Salaries(codeSalarie));

CREATE TABLE EtreAffecte
(codeSalarie VARCHAR(5), codeEquipe VARCHAR(5),
 CONSTRAINT pk_EtreAffecte PRIMARY KEY (codeSalarie, codeEquipe),
 CONSTRAINT fk_EtreAffecte_codeSalarie FOREIGN KEY (codeSalarie) REFERENCES Salaries(codeSalarie),
 CONSTRAINT fk_EtreAffecte_codeEquipe FOREIGN KEY (codeEquipe) REFERENCES Equipes(codeEquipe));

CREATE TABLE Travailler
(codeSalarie VARCHAR(5), dateTravail DATE, codeProjet VARCHAR(5),
 CONSTRAINT pk_Travailler PRIMARY KEY (codeSalarie, dateTravail, codeProjet),
 CONSTRAINT fk_Travailler_codeSalarie FOREIGN KEY (codeSalarie) REFERENCES Salaries(codeSalarie),
 CONSTRAINT fk_Travailler_codeProjet FOREIGN KEY (codeProjet) REFERENCES ProjetsExternes(codePRojet));



/* INSERTION DE DONNEES */

INSERT INTO Salaries (codeSalarie, nomSalarie,prenomSalarie, nbTotalJourneesTravail) (SELECT codeSalarie, nomSalarie,prenomSalarie, nbTotalJourneesTravail FROM Palleja.UNI2_Salaries);
INSERT INTO Equipes (codeEquipe, nomEquipe, codeSalarieChef) (SELECT codeEquipe, nomEquipe, codeSalarieChef FROM Palleja.UNI2_Equipes);
INSERT INTO ProjetsExternes (codeProjet, nomProjet, villeProjet, clientProjet, codeEquipe) (SELECT codeProjet, nomProjet, villeProjet, clientProjet, codeEquipe FROM Palleja.UNI2_ProjetsExternes);
INSERT INTO ProjetsInternes (codeProjet, nomProjet, serviceProjet, codeSalarieResponsable) (SELECT codeProjet, nomProjet, serviceProjet, codeSalarieResponsable FROM Palleja.UNI2_ProjetsInternes);
INSERT INTO EtreAffecte (codeSalarie, codeEquipe) (SELECT codeSalarie, codeEquipe FROM Palleja.UNI2_EtreAffecte);
INSERT INTO Travailler (codeSalarie, dateTravail, codeProjet) (SELECT codeSalarie, dateTravail, codeProjet FROM Palleja.UNI2_Travailler);

COMMIT;





q5
ALTER TABLE Travailler ADD CONSTRAINT Travailler_pas_memeJour UNIQUE (codeSalarie,dateTravail);

q6
ALTER TABLE Equipes DROP CONSTRAINT fk_Equipes_codeSalarieChef;
ALTER TABLE Equipes ADD CONSTRAINT fk_Equipes_codeSalarieChef FOREIGN KEY (codeSalarieChef,codeEquipe) references EtreAffecte(codeSalarie, codeEquipe) ;


q7
create or replace PROCEDURE AjouterJourneeTravail (
    p_codeSalarie Travailler.codeSalarie%TYPE,
    p_codeProjet Travailler.codeProjet%TYPE,
    p_dateTravail Travailler.dateTravail%TYPE) as
begin
    INSERT INTO Travailler (codeSalarie, dateTravail, codeProjet)
    VALUES (p_codeSalarie,p_dateTravail,p_codeProjet);
    UPDATE Salaries set nbTotalJourneesTravail = nbTotalJourneesTravail + 1
    where codeSalarie = p_codeSalarie;
end;

q8
create or replace PROCEDURE AffecterSalarieEquipe (
    p_codeSalarie EtreAffecte.codeSalarie%TYPE,
    p_codeEquipe EtreAffecte.codeEquipe%TYPE) as

    v_nbAffectation int;
begin
    select count(*) into v_nbAffectation
    FROM EtreAffecte
    where codeSalarie = p_codeSalarie;

    if v_nbAffectation < 3 then
        INSERT INTO EtreAffecte (codeSalarie, codeEquipe) VALUES (p_codeSalarie, p_codeEquipe);
    else
        RAISE_APPLICATION_ERROR(-20008, 'Le salarié est déjà affecté à au moins 3 équipes');
    end if;

end;

q9

create or replace PROCEDURE AjouterProjetInterne(
p_codeProjet ProjetsInternes.codeProjet%TYPE ,
p_nomProjet ProjetsInternes.nomProjet%TYPE,
p_serviceProjet ProjetsInternes.serviceProjet%TYPE,
p_codeSalarieResponsable ProjetsInternes.codeSalarieResponsable%TYPE)
as

    v_existeDeja int;
begin
    select count(*) into v_existeDeja
    FROM ProjetsExternes
    where codeProjet = p_codeProjet;

    if v_existeDeja =0 then
        INSERT INTO ProjetsInternes(codeProjet, nomProjet, serviceProjet, codeSalarieResponsable)
        Values (p_codeProjet, p_nomProjet, p_serviceProjet, p_codeSalarieResponsable) ;
    else
        RAISE_APPLICATION_ERROR(-20009, 'Le projet est déjà existant en externe');

    end if;
end;

create or replace PROCEDURE AjouterProjetExterne(
    p_codeProjet ProjetsExternes.codeProjet%TYPE ,
    p_nomProjet ProjetsExternes.nomProjet%TYPE,
    p_villeProjet ProjetsExternes.villeProjet%TYPE,
    p_clientProjet ProjetsExternes.clientProjet%TYPE,
    p_codeEquipe ProjetsExternes.codeEquipe%TYPE)
as

    v_existeDeja int;
begin
    select count(*) into v_existeDeja
    FROM ProjetsInternes
    where codeProjet = p_codeProjet;

    if v_existeDeja =0 then
        INSERT INTO ProjetsExternes(codeProjet, nomProjet, villeProjet, clientProjet, codeEquipe)
        Values (p_codeProjet, p_nomProjet, p_villeProjet, p_clientProjet,p_codeEquipe ) ;
    else
        RAISE_APPLICATION_ERROR(-20010, 'Le projet est déjà existant en interne');

    end if;
end;

q10
create or replace PROCEDURE AjouterJourneeTravail (
    p_codeSalarie Travailler.codeSalarie%TYPE,
    p_codeProjet Travailler.codeProjet%TYPE,
    p_dateTravail Travailler.dateTravail%TYPE) as
    v_affecte int ;
begin
    select  count(*) into v_affecte
    from EtreAffecte
    where codeSalarie = p_codeSalarie
    and codeEquipe in (select codeEquipe
                         from ProjetsInternes
                         where codeProjet = p_codeProjet);

    if v_affecte = 0 then
        RAISE_APPLICATION_ERROR(-20011, 'Le salarie n est pas dans l equipe du projet');
    else
        INSERT INTO Travailler (codeSalarie, dateTravail, codeProjet)
        VALUES (p_codeSalarie,p_dateTravail,p_codeProjet);
        UPDATE Salaries set nbTotalJourneesTravail = nbTotalJourneesTravail + 1
        where codeSalarie = p_codeSalarie;
    end if;
end;






q11
create or replace trigger tr_af_in_totalTravailler
after insert on Travailler
for each row
declare
begin
    update SALARIES set nbTotalJourneesTravail = nbTotalJourneesTravail + 1
    where codeSalarie = :new.codeSalarie;
end;


q12
create or replace trigger tr_be_in_affecter
before insert on EtreAffecte
for each row
declare
v_nbafectation number;
begin
    select count(codeEquipe) into v_nbafectation
    from EtreAffecte
    where CODESALARIE = :new.CODESALARIE;
    if v_nbafectation >= 3 then
    RAISE_APPLICATION_ERROR(-20001, 'Le salarié travaille déja dans 3 equipe');
END IF;
END;

q13
create or replace trigger tr_af_in_totalTravailler
after insert or delete or update of codeSalarie on Travailler
for each row
declare
begin
if (inserting or updating ) then
    update SALARIES set nbTotalJourneesTravail = nbTotalJourneesTravail + 1
    where codeSalarie = :new.codeSalarie;
end if;
if (deleting or updating ) then
    update SALARIES set nbTotalJourneesTravail = nbTotalJourneesTravail - 1
    where codeSalarie = :old.codeSalarie;
end if;
end;

q14
create or replace trigger tr_be_in_projet_ext
before insert on PROJETSEXTERNES
for each row
declare
v_present number;
begin
    select count (codeProjet) into v_present
    from PROJETSINTERNES
    where codeProjet = :new.CODEPROJET;
    if (v_present >= 1 ) then
        RAISE_APPLICATION_ERROR(-20001, 'Le projet est deja interne');
    end if;
    end ;

create or replace trigger tr_be_in_projet_in
before insert on PROJETSINTERNES
for each row
declare
v_present number;
begin
    select count (codeProjet) into v_present
    from PROJETSEXTERNES
    where codeProjet = :new.CODEPROJET;
    if (v_present >= 1 ) then
        RAISE_APPLICATION_ERROR(-20002, 'Le projet est deja externe');
    end if;
end ;

q15
create or replace trigger tr_be_in_travailler
before insert on TRAVAILLER
for each row
declare
v_ds_equipe number;
begin
    select count(codeProjet) into v_ds_equipe
    from PROJETSEXTERNES p
    join ETREAFFECTE e on p.CODEEQUIPE = e.CODEEQUIPE
    where codeSalarie = :new.codeSalarie
    and codeProjet = :new.CODEPROJET;

    if (v_ds_equipe < 1) then
    RAISE_APPLICATION_ERROR(-20003, 'Le salarie ne peut pas traviller sur un projet ou son equipe n est pas affecte');
    end if;
end;

