LIVRES (idLivre, nomLivre, anneeLivre, prixLivre, categorieLivre, idEditeur#)
EDITEURS (idEditeur, nomEditeur, paysEditeur)
ADHERENTS (idAdherent, nomAdherent, prenomAdherent, typeAdherent, idAdherentParrain#)
EMPRUNTS (idEmprunt, dateEmprunt, dateRetour, idLivre#, idAdherent#)


r10
SELECT idLivre
FROM Emprunts
WHERE dateRetour is null

r11
SELECT COUNT ( DISTINCT idLivre)
FROM EMPRUNTS

r12
SELECT nomLivre
FROM LIVRES
WHERE idLivre IN (SELECT idLivre
                    FROM EMPRUNTS
                    JOIN ADHERENTS ON ADHERENTS.idAdherent = EMPRUNTS.idAdherent
                    WHERE nomAdherent = 'Chette' AND prenomAdherent = 'Barbie')


r13
SELECT AVG(prixLivre)
FROM LIVRES
WHERE categorieLivre = 'Informatique'

r14
SELECT nomLivre
FROM LIVRES
WHERE idLivre IN (SELECT idLivre
                    FROM LIVRES
                    JOIN EDITEURS ON EDITEURS.idEditeur = LIVRES.idEditeur
                    WHERE nomEditeur = 'Eyrolles'
                    and prixLivre = (SELECT MIN(prixLivre)
                                    FROM LIVRES
                                    JOIN EDITEURS ON EDITEURS.idEditeur = LIVRES.idEditeur
                                    WHERE nomEditeur = 'Eyrolles'
                                    ))

r15
SELECT prenomAdherent,nomAdherent
FROM ADHERENTS
WHERE idAdherent IN (SELECT idAdherent
                    FROM ADHERENTS
                    MINUS
                    SELECT idAdherent
                    FROM EMPRUNTS)

r16
SELECT prenomAdherent,nomAdherent
FROM ADHERENTS
WHERE idAdherent IN (SELECT idAdherent
                    FROM EMPRUNTS
                    JOIN LIVRES ON LIVRES.idLivre = EMPRUNTS.idLivre
                    WHERE nomLivre = 'UML pour les Ninjas'
                    UNION 
                    SELECT idAdherent
                    FROM EMPRUNTS
                    JOIN LIVRES ON LIVRES.idLivre = EMPRUNTS.idLivre
                    WHERE nomLivre = 'Coder Proprement'
                    UNION )



r17
SELECT nomEditeur,paysEditeur, COUNT(idLivre)
FROM EDITEURS
LEFT JOIN LIVRES ON LIVRES.idEditeur = EDITEURS.idEditeur
GROUP BY nomEditeur,paysEditeur


r18
SELECT nomLivre
FROM LIVRES
WHERE idLivre IN (SELECT idLivre
                    FROM EMPRUNTS
                    GROUP BY idLivre
                    HAVING COUNT(*) >3)


r19
SELECT nomAdherent,prenomAdherent
FROM ADHERENTS a
WHERE NOT EXISTS (SELECT LIVRES.idLivre
                FROM EMPRUNTS
                JOIN LIVRES ON LIVRES.idLivre = EMPRUNTS.idLivre
                WHERE categorieLivre != 'Gestion'
                AND EMPRUNTS.idAdherent = a.idAdherent)
AND idAdherent IN (SELECT idAdherent
                    FROM EMPRUNTS)

r20
SELECT nomAdherent
FROM ADHERENTS
WHERE NOT EXISTS (SELECT LIVRES.idLivre
                FROM LIVRES
                WHERE categorieLivre = 'Gestion'
                MINUS
                SELECT idLivre
                FROM EMPRUNTS
                WHERE EMPRUNTS.idAdherent = ADHERENTS.idAdherent)

r21
SELECT nomAdherent,prenomAdherent
FROM ADHERENTS
WHERE idAdherentParrain IS null
AND typeAdherent = 'Etudiant'

r22
SELECT nomAdherent,prenomAdherent
FROM ADHERENTS


r23 ///////////////
SELECT nomAdherent,prenomAdherent
FROM ADHERENTS
WHERE idAdherent IN (SELECT idAdherent
                    FROM EMPRUNTS
                    GROUP BY idAdherent
                    HAVING MAX(*))